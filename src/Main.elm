module Main exposing (main)

import Browser exposing (Document, UrlRequest)
import Browser.Navigation as Nav
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra as Decode
import Json.Encode as Encode
import List.Extra as List
import Maybe exposing (Maybe)
import Platform.Cmd as Cmd
import Tuple exposing (pair)
import Url exposing (Url)


baseUrl : String
baseUrl =
    "http://localhost:8080/api"


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }


type alias ID =
    Int


type alias Project =
    { name : String }


type alias Task =
    { name : String
    , projectId : ID
    , completed : Bool
    }


type alias Model =
    { projects : Dict ID Project
    , tasks : Dict ID Task
    , url : Url
    , key : Nav.Key
    }


type Msg
    = LinkClicked UrlRequest
    | UrlChanged Url
    | ProjectsReceived (Result Http.Error (Dict ID Project))
    | TasksReceived (Result Http.Error (Dict ID Task))
    | TaskSent (Result Http.Error ())
    | Complete ID Bool


postNoContent : String -> Http.Body -> Http.Request ()
postNoContent url body =
    Http.request
        { method = "POST"
        , headers = []
        , url = url
        , body = body
        , expect = Http.expectStringResponse (\_ -> Ok ())
        , timeout = Nothing
        , withCredentials = False
        }


getProjects : Http.Request (Dict ID Project)
getProjects =
    Http.get (baseUrl ++ "/projects") decodeProjects


getTasks : Http.Request (Dict ID Task)
getTasks =
    Http.get (baseUrl ++ "/tasks") decodeTasks


postTask : ( Int, Task ) -> Http.Request ()
postTask ( id, task ) =
    postNoContent
        (baseUrl ++ "/tasks/" ++ String.fromInt id)
        (Http.jsonBody (encodeTask ( id, task )))


postTasks : Dict ID Task -> Http.Request ()
postTasks tasks =
    postNoContent
        (baseUrl ++ "/tasks")
        (Http.jsonBody (encodeTasks tasks))


postProject : ( Int, Project ) -> Http.Request ()
postProject ( id, project ) =
    postNoContent
        (baseUrl ++ "/projects/" ++ String.fromInt id)
        (Http.jsonBody (encodeProject ( id, project )))


postProjects : Dict ID Project -> Http.Request ()
postProjects projects =
    postNoContent
        (baseUrl ++ "/projects")
        (Http.jsonBody (encodeProjects projects))


decodeTask : Decoder Task
decodeTask =
    Decode.succeed Task
        |> Decode.andMap (Decode.field "name" Decode.string)
        |> Decode.andMap (Decode.field "projectId" Decode.int)
        |> Decode.andMap (Decode.field "completed" Decode.bool)


decodeTasks : Decoder (Dict ID Task)
decodeTasks =
    Decode.succeed Tuple.pair
        |> Decode.andMap (Decode.field "id" Decode.int)
        |> Decode.andMap decodeTask
        |> Decode.list
        |> Decode.map Dict.fromList


decodeProject : Decoder Project
decodeProject =
    Decode.succeed Project
        |> Decode.andMap (Decode.field "name" Decode.string)


decodeProjects : Decoder (Dict ID Project)
decodeProjects =
    Decode.succeed Tuple.pair
        |> Decode.andMap (Decode.field "id" Decode.int)
        |> Decode.andMap decodeProject
        |> Decode.list
        |> Decode.map Dict.fromList


encodeProject : ( ID, Project ) -> Encode.Value
encodeProject ( id, project ) =
    Encode.object
        [ ( "id", Encode.int id )
        , ( "name", Encode.string project.name )
        ]


encodeProjects : Dict ID Project -> Encode.Value
encodeProjects projects =
    Encode.list encodeProject (Dict.toList projects)


encodeTask : ( ID, Task ) -> Encode.Value
encodeTask ( id, task ) =
    Encode.object
        [ ( "id", Encode.int id )
        , ( "name", Encode.string task.name )
        , ( "projectId", Encode.int task.projectId )
        , ( "completed", Encode.bool task.completed )
        ]


encodeTasks : Dict ID Task -> Encode.Value
encodeTasks tasks =
    Encode.list encodeTask (Dict.toList tasks)


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( { projects = Dict.empty
      , tasks = Dict.empty
      , url = url
      , key = key
      }
    , Cmd.batch
        [ getProjects |> Http.send ProjectsReceived
        , getTasks |> Http.send TasksReceived
        ]
    )


viewProject : Dict ID Task -> ( ID, Project ) -> Html Msg
viewProject tasks ( id, project ) =
    Html.div []
        [ Html.h2 [] [ Html.text project.name ]
        , Html.div [] (List.map viewTask (Dict.toList <| Dict.filter (\_ t -> t.projectId == id) <| tasks))
        ]


viewTask : ( ID, Task ) -> Html Msg
viewTask ( id, task ) =
    Html.div []
        [ Html.input
            [ Attrs.type_ "checkbox"
            , Attrs.checked task.completed
            , Events.onCheck (Complete id)
            ]
            []
        , Html.label [] [ Html.text (String.fromInt id ++ ": " ++ task.name) ]
        ]


view : Model -> Document Msg
view model =
    { title = "Bikeshed"
    , body =
        [ Html.h1 [] [ Html.text "Bikeshed" ] ]
            ++ List.map (viewProject model.tasks) (Dict.toList model.projects)
    }


completeTask : ID -> Bool -> Dict ID Task -> Dict ID Task
completeTask id completed tasks =
    Dict.update id (Maybe.map (\t -> { t | completed = completed })) tasks


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url }, Cmd.none )

        TasksReceived result ->
            case result of
                Err e ->
                    Debug.log "Failed to load tasks" e
                        |> always ( model, Cmd.none )

                Ok ts ->
                    ( { model | tasks = ts }, Cmd.none )

        ProjectsReceived result ->
            case result of
                Err e ->
                    Debug.log "Failed to load projects" e
                        |> always ( model, Cmd.none )

                Ok ps ->
                    ( { model | projects = ps }, Cmd.none )

        TaskSent result ->
            case result of
                Err e ->
                    Debug.log "Failed to send task" e
                        -- |> always ( model, Nav.reload )
                        |> always ( model, Cmd.none )

                Ok () ->
                    ( model, Cmd.none )

        Complete id completed ->
            case Dict.get id model.tasks of
                Just task ->
                    ( { model | tasks = completeTask id completed model.tasks }
                    , postTask ( id, { task | completed = completed } ) |> Http.send TaskSent
                    )

                Nothing ->
                    Debug.log ("Task with id " ++ String.fromInt id ++ "not found") ()
                        |> always ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
